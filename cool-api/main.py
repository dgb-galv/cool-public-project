from fastapi import FastAPI
# from db import CoolQueries


app = FastAPI()

@app.get("/api/cool_cats")
def get_cool_cats():
    # def get_cool_cats(queries: CoolQueries = Depends()):
    cool_cats = [
        "Bob Dylan",
        "Bob Marley",
        "Neighbor Bob"
    ]
    # cool_cats = queries.get_cool_cats()
    return cool_cats
